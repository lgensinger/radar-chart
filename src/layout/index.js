import { degreeToRadian, largestDivisor, SeriesData } from "@lgv/visualization-chart";
import { group, max } from "d3-array";
import { scaleLinear, scalePoint } from "d3-scale";

/**
 * RadarLayout is a data abstraction.
 * @param {integer} axesStart - degree value of where to start axes clockwise starting at 0 == top center of circle
 * @param {object} data - usually array; occasionally object
 * @param {integer} grid - number of rings in polar grid
 */
class RadarLayout extends SeriesData {

    constructor(data, grid, axesStart) {

        // initialize inheritance
        super(data);

        // update self
        this.axesStart = axesStart;
        this.grid = grid;
        this.radius = null;

    }

    /**
     * Extract axis labels.
     * @returns An array of strings where each represents an axis.
     */
    get axes() {
        return [ ...new Set(Object.keys(this.data).map(k => Object.keys(this.data[k]).map(kk => this.data[k][kk].label)).flat())].sort((a,b) => a.localeCompare(b));
    }

    /**
     * Generate axis points to render labels at the polar edge.
     * @returns An array of objects where each represetns a coordinate with angle/radius for d3 render.
     */
    get axesLabels() {
        return this.axes.map((d,i) => ({
            label: d,
            angle: degreeToRadian(this.axesStart - 90 + (this.axisAngle * i)),
            radius: this.radius
        }));
    }

    /**
     * Determine angle of each axis.
     * @returns A float representing an angle in degrees.
     */
    get axisAngle() {
        return 360 / this.axes.length;
    }

    /**
     * Construct a scale to map labels along axis.
     * @returns A d3 scale function.
     */
    get axisScale() {
        return scalePoint()
            .domain(this.grid ? [0].concat(this.grid.map((d,i) => i + 1)) : [0].concat([...Array(largestDivisor(max(this.values.flat()))).keys()].map(d => this.valueScale.domain()[1] / (d + 1)).sort((a,b) => a - b)))
            .range([this.radius, 0]);
    }

    /**
     * Generate axis points to render lines.
     * @returns An array of [[x1,y1],[x2,y2]] where each subarray are start/end points of a line.
     */
    get lines() {
        return this.axes.map((d,i) => ({
            id: this.axes.indexOf(d),
            label: d,
            points: [
                [
                    degreeToRadian(this.axesStart),
                    0
                ],
                [
                    degreeToRadian(this.axesStart + (this.axisAngle * i)),
                    this.radius
                ]
            ]
        }));
    }

    /**
     * Generate series points to render polygons.
     * @returns An array of [a,r] where a is the angle in radians and r is the radius for a given point.
     */
    get points() {
        return Object.keys(this.data).map(k => this.axes.map((kk,i) => ({
            id: i,
            angle: degreeToRadian(this.axesStart + (this.axisAngle * i)),
            axis: kk,
            series: k,
            value: this.data[k][kk].value
        }))).flat();
    }

    /**
     * Generate polygon values.
     * @returns An array of objects where each represents a polygon.
     */
    get polygons() {
        return Array.from(group(this.points, d => d.series), d => ({
            series: d[0],
            points: d[1]
        }));
    }

    /**
     * Construct a scale to map value along axis.
     * @returns A d3 scale function.
     */
    get valueScale() {
        return scaleLinear()
            .domain([0, max(this.values.flat())])
            .range([0, this.radius]);
    }

    /**
     * Generate series values.
     * @returns An array of arrays of floats where each represents a single axis value for a given series.
     */
    get values() {
        return Object.keys(this.data).map(k => Object.keys(this.data[k]).map(kk => this.data[k][kk].value));
    }

    /**
     * Generate event detail object.
     * @param {d} object - d3.js data object
     * @param {e} Event - JavaScript Event object
     * @returns An object of key/value metadata pairs.
     */
    configureEventDetails(d,e) {
        return {
            id: this.extractId(d),
            axis: d.axis,
            series: this.extractSeries(d),
            value: this.extractValue(d),
            valueLabel: this.extractValueAsLabel(d),
            xy: [e.clientX, e.clientY]
        };

    }

}

export { RadarLayout };
export default RadarLayout;
