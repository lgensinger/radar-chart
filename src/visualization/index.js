import { ChartLabel, RadialGrid, radianToDegree } from "@lgv/visualization-chart";
import { axisRight } from "d3-axis";
import { select } from "d3-selection";
import { radialLine } from "d3-shape";
import { transition } from "d3-transition";

import { configuration, configurationLayout } from "../configuration.js";
import { RadarLayout as RL } from "../layout/index.js";

/**
 * RadarChart is an multivariate part-of-a-whole visualization.
 * @param {float} axesStart - angle in degrees to start axis clockwise around center where 0 is top center of the circle
 * @param {array} data - objects where each represents a path in the hierarchy
 * @param {array} grid - strings where each represents a label for polar grid concentric circles
 * @param {float} radiusPoint - radius of point
 * @param {integer} height - artboard height
 * @param {integer} width - artboard width
 */
class RadarChart extends RadialGrid {
    constructor(data, width=configurationLayout.width, height=configurationLayout.height, RadarLayout=null, grid=null, axesStart=configurationLayout.axesStart, radiusPoint=null, label=configuration.branding, name=configuration.name) {

        // initialize inheritance
        super(data, width, height, RadarLayout ? RadarLayout : new RL(data, grid, axesStart), label, name);

        // update self
        this.axis = null;
        this.axisGrid = null;
        this.classAxis = `${label}-axis`;
        this.classAxisGrid = `${label}-axis-grid`;
        this.classAxisLabel = `${label}-axis-label`;
        this.classGrid = `${label}-grid`;
        this.classPoint = `${label}-point`;
        this.classPolygon = `${label}-polygon`;
        this.grid = null;
        this.labelAxis = null;
        this.point = null;
        this.polygon = null;
        this.radiusPoint = radiusPoint || this.unit / 4;

        // have to set layout functions after initialization
        this.Data.radius = this.radius;

    }

    /**
     * Construct axis.
     * @returns A d3 axis function.
     */
    get axisRight() {
        return axisRight(this.Data.axisScale)
            .tickFormat((d,i) => this.Data.grid ? (i == 0 ? null : this.Data.grid[i-1]) : d);
    }

    /**
     * Calculate radius.
     * @returns A float representing the radius of the outer most circle.
     */
    get radius() {
        return Math.min( ...[this.height, this.width]) * 0.35;
    }

    /**
     * Position and minimally style labels in SVG dom element.
     */
    configureAxes() {
        this.axis
            .transition().duration(1000)
            .attr("class", this.classAxis)
            .attr("data-label", d => this.Data.extractLabel(d))
            .attr("d", d => radialLine()(d.points))
            .attr("stroke", "lightgrey");
    }

    /**
     * Position and minimally style axis labels in SVG dom element.
     */
    configureAxesLabels() {
        this.labelAxis
            .transition().duration(1000)
            .attr("class", this.classAxisLabel)
            .attr("x", d => (d.radius * 1.3) * Math.cos(d.angle))
            .attr("y", d => (d.radius * 1.3) * Math.sin(d.angle))
            .attr("text-anchor", d => this.Label.determineAlignment(radianToDegree(d.angle)))
            .text(d => d.label);
    }

    /**
     * Position and minimally style grid in SVG dom element.
     */
    configureGrid() {
        this.grid
            .transition().duration(1000)
            .attr("class", this.classGrid)
            .attr("data-value", d => this.Data.axisScale.domain().sort((a,b) => b - a).indexOf(d))
            .attr("cx", 0)
            .attr("cy", 0)
            .attr("r", d => this.Data.axisScale(d))
            .attr("fill", "transparent")
            .attr("stroke", "lightgrey");
    }

    /**
     * Position and minimally style grid axis line in SVG dom element.
     */
    configureAxisGrid() {
        this.axisGrid
            .transition().duration(1000)
            .attr("class", this.classAxisGrid)
            .attr("transform", `translate(0,${-this.radius})`)
            .call(this.axisRight);
    }

    /**
     * Expose element events to dom.
     */
    configurePointEvents() {
        this.point
            .on("click", (e,d) => this.configureEvent("point-click",d,e))
            .on("mouseover", (e,d) => {

                // update class
                select(e.target).attr("class", `${this.classPoint} active`);

                // send event to parent
                this.configureEvent("point-mouseover",d,e);

            })
            .on("mouseout", (e,d) => {

                // update class
                select(e.target).attr("class", this.classPoint);

                // send event to parent
                this.artboard.dispatch("point-mouseout", {
                    bubbles: true
                });

            });
    }

    /**
     * Position and minimally style series polygons in SVG dom element.
     */
    configurePoints() {
        this.point
            .transition().duration(1000)
            .attr("class", this.classPoint)
            .attr("data-value", d => d.value)
            .attr("data-axis", d => d.axis)
            .attr("data-series", d => d.series)
            .attr("cx", d => this.Data.valueScale(d.value) * Math.cos(d.angle - (Math.PI / 2)))
            .attr("cy", d => this.Data.valueScale(d.value) * Math.sin(d.angle - (Math.PI / 2)))
            .attr("r", this.radiusPoint)
            .attr("fill", "lightgrey");
    }

    /**
     * Position and minimally style series polygons in SVG dom element.
     */
    configurePolygons() {
        this.polygon
            .transition().duration(1000)
            .attr("class", this.classPolygon)
            .attr("data-series", d => this.Data.extractSeries(d))
            .attr("d", d => `${radialLine()(d.points.map(x => [x.angle,this.Data.valueScale(x.value)]))}Z`)
            .attr("fill", "transparent")
            .attr("stroke", "currentColor");
    }

    /**
     * Generate axes in SVG element.
     * @param {node} domNode - d3.js SVG selection
     */
    generateAxes(domNode) {
        return domNode
            .selectAll(`.${this.classAxis}`)
            .data(this.Data.lines)
            .join(
                enter => enter.append("path"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate axes labels in SVG element.
     * @param {node} domNode - d3.js SVG selection
     */
    generateAxesLabels(domNode) {
        return domNode
            .selectAll(`.${this.classAxisLabel}`)
            .data(this.Data.axesLabels)
            .join(
                enter => enter.append("text"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate grid axis in SVG element.
     * @param {node} domNode - d3.js SVG selection
     */
    generateAxisGrid(domNode) {
        return domNode
            .selectAll(`.${this.classAxisGrid}`)
            .data(d => [d])
            .join(
                enter => enter.append("g"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate visualization.
     */
    generateChart() {

        // initialize labeling
        this.Label = new ChartLabel(this.artboard, this.Data);

        // generate circular grid
        this.grid = this.generateGrid(this.content);
        this.configureGrid();

        // generate grid axis
        this.axisGrid = this.generateAxisGrid(this.content);
        this.configureAxisGrid();

        // generate polygons
        this.polygon = this.generatePolygons(this.content);
        this.configurePolygons();

        // generate axes
        this.axis = this.generateAxes(this.content);
        this.configureAxes();

        // generate points
        this.point = this.generatePoints(this.content);
        this.configurePoints();
        this.configurePointEvents();

        // generate axis labels
        this.labelAxis = this.generateAxesLabels(this.content);
        this.configureAxesLabels();

    }

    /**
     * Construct circular grid in HTML DOM.
     * @param {node} domNode - HTML node
     * @returns A d3.js selection.
     */
    generateGrid(domNode) {
        return domNode
            .selectAll(`.${this.classGrid}`)
            .data(this.axisRight.scale().domain())
            .join(
                enter => enter.append("circle"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate data points of polygon in SVG element.
     * @param {node} domNode - d3.js SVG selection
     */
    generatePoints(domNode) {
        return domNode
            .selectAll(`.${this.classPoint}`)
            .data(this.Data.points)
            .join(
                enter => enter.append("circle"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate series polygons in SVG element.
     * @param {node} domNode - d3.js SVG selection
     */
    generatePolygons(domNode) {
        return domNode
            .selectAll(`.${this.classPolygon}`)
            .data(this.Data.polygons)
            .join(
                enter => enter.append("path"),
                update => update,
                exit => exit.remove()
            );

    }

};

export { RadarChart };
export default RadarChart;
