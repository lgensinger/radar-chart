import test from "ava";

import { configuration, configurationLayout } from "../src/configuration.js";
import { RadarChart } from "../src/index.js";

/******************** EMPTY VARIABLES ********************/

// initialize
let rc = new RadarChart();

// TEST INIT //
test("init", t => {

    t.true(rc.height === configurationLayout.height);
    t.true(rc.width === configurationLayout.width);

});

// TEST RENDER //
/*test("render", t => {

    // clear document
    document.body.innerHTML = "";

    // render to dom
    rc.render(document.body);

    // get generated element
    let artboard = document.querySelector(`.${configuration.name}`);

    t.true(artboard !== undefined);
    t.true(artboard.nodeName == "svg");
    t.true(artboard.getAttribute("viewBox").split(" ")[3] == rc.artboardHeight);
    t.true(artboard.getAttribute("viewBox").split(" ")[2] == rc.artboardWidth);

});*/

/******************** DECLARED PARAMS ********************/

let testWidth = Math.floor(Math.random() * 500);
let testHeight = Math.floor(Math.random() * 500);

let s = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
let a = "abcdefghijklmnopqrstuvwxyz";
let seriesCount = 10;
let axisCount = 5;
let testData = {};
[...Array(Math.floor((Math.random() * seriesCount) + 3)).keys()].forEach(d => {
    testData[s[d]] = {};
    [...Array(Math.floor((Math.random() * axisCount) )).keys()].forEach(x => testData[s[d]][a[x]] = Math.floor(Math.random() * 5) + 5);
});

// initialize
let rct = new RadarChart(testData, testWidth, testHeight);

// TEST INIT //
test("init_params", t => {

    t.true(rct.height === testHeight);
    t.true(rct.width === testWidth);

});

// TEST RENDER //
/*test("render_params", t => {

    // clear document
    document.body.innerHTML = "";

    // render to dom
    rct.render(document.body);

    // get generated element
    let artboard = document.querySelector(`.${configuration.name}`);

    t.true(artboard !== undefined);
    t.true(artboard.nodeName == "svg");
    t.true(artboard.getAttribute("viewBox").split(" ")[3] == rct.artboardHeight);
    t.true(artboard.getAttribute("viewBox").split(" ")[2] == rct.artboardWidth);

});*/
