# Radar Chart

ES6 d3.js radar chart visualization.

## Install

```bash
# install package
npm install @lgv/radar-chart
```

## Data Format

The following values are the expected input data structure.

```json
{
    "series1": { "a": 6, "b": 10, "c": 30 },
    "series2": { "a": 10, "b": 40, "c": 5 },
    "series3": { "a": 0, "b": 3, "c": 2 }
}
```

## Use Module

```bash
import { RadarChart } from "@lgv/radar-chart";

// have some data
let data = {
    "series1": { "a": 6, "b": 10, "c": 30 },
    "series2": { "a": 10, "b": 40, "c": 5 },
    "series3": { "a": 0, "b": 3, "c": 2 }
}

// initialize
const rc = new RadarChart(data);

// render visualization
rc.render(document.body);
```

## Environment Variables

The following values can be set via environment or passed into the class.

| Name | Type | Description |
| :-- | :-- | :-- |
| `LGV_LAYOUT_AXES_START` | float | degree start position where 0 == 12 o'clock |
| `LGV_HEIGHT` | integer | height of artboard |
| `LGV_WIDTH` | integer | width of artboard |

## Style

Style is expected to be addressed via css. Any style not met by the visualization module is expected to be added by the importing component.

| Class | Element |
| :-- | :-- |
| `lgv-radar-chart` | top-level svg element |
| `lgv-axis` | radar axis element |
| `lgv-axis-grid` | axis style for polar grid |
| `lgv-axis-label` | axis label |
| `lgv-grid` | circular grid element |
| `lgv-point` | polygon point |
| `lgv-polygon` | radar series polygon element |

## Actively Develop

```bash
# clone repository
git clone <repo_url>

# update directory context
cd radar-chart

# run docker container
docker run \
  --rm \
  -it  \
  -v $(pwd):/project \
  -w /project \
  -p 8080:8080 \
  node \
  bash

# FROM INSIDE RUNNING CONTAINER

# install module
npm install .

# run development server
npm run startdocker

# edit src/index.js
# add const rc = new RadarChart(data);
# add rc.render(document.body);
# replace `data` with whatever data you want to develop with

# view visualization in browser at http://localhost:8080
```
